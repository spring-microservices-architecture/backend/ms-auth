package com.mycompany.msauth.integracion.resources;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.mycompany.msauth.core.accesodato.entity.Usuario;
import com.mycompany.msauth.core.service.UsuarioService;
import com.mycompany.msauth.integracion.dto.TokenDto;
import com.mycompany.msauth.integracion.dto.UsuarioDto;

@RestController
@RequestMapping("/auth")
public class AuthUserController {

	@Autowired
    UsuarioService usuarioService;

	
    @PostMapping("/login")
    public ResponseEntity<TokenDto> login(@RequestBody UsuarioDto dto){
        TokenDto tokenDto = usuarioService.login(dto);
        if(tokenDto == null)
            return ResponseEntity.status(401).build();
        
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("access-control-expose-headers", "Authorization");
        responseHeaders.set("Authorization", "Bearer "+tokenDto.getToken());
        return ResponseEntity.ok().headers(responseHeaders).body(tokenDto);
    }

    @PostMapping("/validate")
    public ResponseEntity<TokenDto> validate(@RequestParam String token){
        TokenDto tokenDto = usuarioService.validate(token);
        if(tokenDto == null)
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(tokenDto);
    }

    @PostMapping("/create")
    public ResponseEntity<Usuario> create(@RequestBody UsuarioDto dto){
    	Usuario authUser = usuarioService.save(dto);
        if(authUser == null)
            return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(authUser);
    }
}