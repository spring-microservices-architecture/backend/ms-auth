package com.mycompany.msauth.core.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.mycompany.msauth.core.accesodato.entity.Usuario;
import com.mycompany.msauth.core.interfaces.UsuarioRepository;
import com.mycompany.msauth.integracion.dto.TokenDto;
import com.mycompany.msauth.integracion.dto.UsuarioDto;
import com.mycompany.msauth.security.JwtProvider;

@Service
public class UsuarioService {

	@Autowired
	UsuarioRepository usuarioRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtProvider jwtProvider;
    
    public Usuario save(UsuarioDto dto) {
    	System.out.println("USUARIO "+dto.getUsuario());
        Optional<Usuario> user = usuarioRepository.findByUsuario(dto.getUsuario());
        if(user.isPresent())
            return null;
        String password = passwordEncoder.encode(dto.getClave());
        Usuario authUser = new Usuario();
        authUser.setUsuario(dto.getUsuario());
        authUser.setClave(password);
        return usuarioRepository.save(authUser);
    }

    public TokenDto login(UsuarioDto dto) {
        Optional<Usuario> user = usuarioRepository.findByUsuario(dto.getUsuario());
        if(!user.isPresent())
            return null;
        if(passwordEncoder.matches(dto.getClave(), user.get().getClave()))
            return new TokenDto(jwtProvider.createToken(user.get()));
        return null;
    }

    public TokenDto validate(String token) {
        if(!jwtProvider.validate(token))
            return null;
        String username = jwtProvider.getUserNameFromToken(token);
        if(!usuarioRepository.findByUsuario(username).isPresent())
            return null;
        return new TokenDto(token);
    }
}